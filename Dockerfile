﻿FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Directory.Build.props", "."]
COPY ["src/ITArchitect.DevOps.Web.API/ITArchitect.DevOps.Web.API.csproj", "ITArchitect.DevOps.Web.API/"]
COPY ["src/ITArchitect.DevOps.Infrastructure/ITArchitect.DevOps.Infrastructure.csproj", "ITArchitect.DevOps.Infrastructure/"]
COPY ["src/ITArchitect.DevOps.Domain/ITArchitect.DevOps.Domain.csproj", "ITArchitect.DevOps.Domain/"]
COPY ["src/ITArchitect.DevOps.Web/ITArchitect.DevOps.Web.csproj", "ITArchitect.DevOps.Web/"]
COPY ["src/ITArchitect.DevOps.Application/ITArchitect.DevOps.Application.csproj", "ITArchitect.DevOps.Application/"]
RUN dotnet restore "ITArchitect.DevOps.Web.API/ITArchitect.DevOps.Web.API.csproj"
COPY ["src/", "."]
WORKDIR "/src/ITArchitect.DevOps.Web.API"
RUN dotnet build "ITArchitect.DevOps.Web.API.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "ITArchitect.DevOps.Web.API.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ITArchitect.DevOps.Web.API.dll"]
