# ITArchitect.DevOps

Проект для обучения тестированию, сборки и разворачивания в рамках курса "Архитектор информационных систем" (модуль "Развёртывание, сопровождение, поддержка"). Представляет собой web-приложение (возможно с использованием БД, кэша и очереди сообщений/событий).

## Окружения развертывания

Планируется использовать Development-, Test-, Staging-, и Production-окружения. При этом Development-окружение (вероятнее всего) будет в виде `docker compose` либо локального кластера Kubernetes из одной ноды (minikube или Docker Desktop), а остальные в виде отдельных кластеров Kubernetes, при этом Staging-окружение должно быть максимально соответствовать Production-окружению.

## Структура проекта

Проект представляет собой решение на языке C# (.NET 6) и состоит из нескольких проектов. 

![Структура решения](docs/ITArchitect.DevOps.diagram.png)

- `ITArchitect.DevOps.Domain` - проект с доменной логикой, сущностями и т.п.
  - `ITArchitect.DevOps.Domain.Tests` - проект с unit-тестами доменной логики
- `ITArchitect.DevOps.Application` - проект с логикой приложения (командами, запросами и их обработчиками)
  - `ITArchitect.DevOps.Application.Tests` - проект с unit-тестами логики приложения
- `ITArchitect.DevOps.Infrastructure` - проект инфраструктурного слоя (реализации работы с БД, кэшами, очередями и т.п.)
- `ITArchitect.DevOps.Web` - проект, общий для web-приложений (контроллеры, фильтры и т.п.)
- `ITArchitect.DevOps.Web.API` - точка входа в приложение в виде REST API

## Структура папок

```
├───docs
├───src
│   ├───ITArchitect.DevOps.Application
│   ├───ITArchitect.DevOps.Domain
│   ├───ITArchitect.DevOps.Infrastructure
│   ├───ITArchitect.DevOps.Web
│   └───ITArchitect.DevOps.Web.API
└───test
    ├───ITArchitect.DevOps.Application.Tests
    └───ITArchitect.DevOps.Domain.Tests
```